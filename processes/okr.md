# How-To: Handle OKRs

## Planning

TBD

## Issue Organization

1. Create the OKR Work Issue/Epic. This item is the primary, context-establishing entry point for the project. Its audience is technical, and it should be constructed so that an unfamiliar party can gather enough information, and be directed to the appropriate child/related issues, to speak confidently about the work, or to repeat it entirely.

    ```markdown

    ## Context
    
    A summary of events, discussions, and/or problems that has led to this work being proposed. This is the information required to understand why the Problem is actually a problem.

    ## Problem

    A summary of what this work is attempting to solve, or put another way, what the business impact will be if we do not do this work.

    ## Goal

    The quantifiable, customer-oriented change that is desired as an outcome of this work.

    ## Requirements

    Any additional criteria that must be met in the course of achieving the Goal.

    ## DRI

    Tag the DRI for the project here. Can also include Participants here as a subheading, if desired.

    ## Milestones

    Overall plan, and links to child/related Issues for the iterative deliverables of the project.
    ```

2. Update the Key Result item description. The KR itself is usually created by your manager, and they will ping you to update the description when it is ready. This item is a high-level summary of the work's justification, company impact, and steps taken to pursue the work through the quarter. Its audience is executive, and should be constructed to emphasize the project's context, company impact, and high-level summary of progress, so that leadership can track its progress, understand or alleviate blockers, and make prioritization decisions when necessary.

    ```markdown
    ## Context

    (You can copy the Context section from the Work Issue/Epic.)

    ## Problem

    (You can copy the Problem section from the Work Issue/Epic.)

    ## Goal

    (You can copy the Goal section from the Work Issue/Epic.)

    ## Related Epic

    (Replace with a link to the primary OKR Work Issue/Epic)

    ## Steps

    A summarized version of the Milestones section from the Work Issue/Epic.

    ```

## In-Quarter Progress Updates

Progress updates should be posted to the Key Result item weekly, on Friday.

Use the following template:

```markdown
## Status update - Week of YYYY-MM-DD

Progress was updated to x%.

### What was done :white_check_mark:

* 

### Next steps :construction_worker:

* 

### Blockers :octagonal_sign:

*
```
