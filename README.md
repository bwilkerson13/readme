# Brittany Wilkerson's readme

## My Role at GitLab

My title is **Senior Software Engineer in Test, Dedicated - Environment Automation**.

I work on the Self-Managed Platforms Team, and partner closely with the GitLab Dedicated team to facilitate their use of our projects like  [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/tree/main), [GitLab Performance Tool](https://gitlab.com/gitlab-org/quality/performance/-/tree/main/), and [Reference Architectures](https://docs.gitlab.com/ee/administration/reference_architectures/).

## Who I Am

I really like computers, and one day I'll figure out how they work! :wink:

I have a B.S. in Robotics and Embedded Systems, and throughout my career I've held almost every sort of software engineer position that there is.

I play video games.

I try to read at least one book a week.

I enjoy gardening, and am embarking on a permaculture project to mostly reclaim my yard to native forest and edible plants.

I love storytelling, and tabletop RPGs, and I'm working on my own setting and campaign.

## How I Work

My timezone is US Central Time. I tend to work between 7:00 AM to 3:00 PM, with a break around 11:00 for lunch.
